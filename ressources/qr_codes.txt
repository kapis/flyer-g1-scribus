Les QR ne sont pas des images externes. Pour les remplacer dans le fichier Scribus, vous devez passer au calque Images, puis insérer le code-barres dans le menu "Insertion" de Scribus.

Los QR no son imágenes externas. Para remplazarlos dentro del fichero Scribus, has de cambiar a capa de Imágenes y luego insertar Código de Barras desde el menú "Insertar" de Scribus.

The QR codes are not external images. To replace them inside scribus, you have to go to the Image layer, delete the current QR and afterwards insert a new Barcode from the menu "Insert/Add" within Scribus.
