#!/usr/bin/env python3

import json

f = open("du-graph.json", "r")
data = json.load(f)
f.close()

output = [[i, data[0]["points"][i][1], data[4]["points"][i][1], data[2]["points"][i][1]] for i in range(160)]

f = open("du-graph.dat", "w")
f.write("\n".join([" ".join([str(i) for i in line]) for line in output])+"\n")
f.close()
